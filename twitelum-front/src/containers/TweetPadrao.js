import Tweet from '../components/Tweet'
import { connect } from 'react-redux'
import * as TweetsAPI from '../api/TweetsAPI'

/*
class tweetPadrao extends Component {
    funcaoRemove () { store.dispatch(TweetsAPI.remove())
    }
    render(){
        return (
            <Tweet removeHandler = { removeHandler} />
        )
    }
}*/

const mapDispatchToProps = (dispatch, propsRecebidas) => {
    return {
        removeHandler: () => {
            dispatch(TweetsAPI.remove(propsRecebidas.tweetInfo._id))
        },
        handleLike: () => {
            dispatch(TweetsAPI.like(propsRecebidas.tweetInfo._id))
        }
    }
}

const tweetPadraoContainer = connect(null, mapDispatchToProps)(Tweet)

export default tweetPadraoContainer

//removeHandler={() => this.removeTweet(tweetInfo._id)}

/*
Exemplo

function calculadora(numero1, numero2) {
    return function (numero3) {
        return numero1 + numero2
    }
}

calculadora (2, 3) (4)

*/