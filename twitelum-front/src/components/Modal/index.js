import React, { Component } from 'react'
import './index.css'

class Modal extends Component {

    render(){
        return (
            <div className={`modal ${this.props.isAberto ? 'modal__active' : ''}`} 
            onClick={this.props.fechaModal}>
                { this.props.isAberto &&
                    <div className="modal__wrap">
                        { this.props.children}</div>
                    }
            </div>
        )
    }
}

export default Modal