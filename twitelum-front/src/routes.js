import React, { Component } from 'react'
import { Switch, Route } from 'react-router-dom'
import Home from './pages/Home'
import LoginPage from './pages/LoginPage'
import Redirect from 'react-router-dom/Redirect';

function estaAutenticado() {
    if (localStorage.getItem('TOKEN')) {
        return true
    } else {
        return false
    }
}

class PrivateRoute extends Component {
    render() {
        //const Component = this.props.component
        //const props = this.props
        if (estaAutenticado()) {
            return (
            <Route {...this.props} /> )
        } else {
            return (
                <Redirect to="/login" />)
        }
    }
}

export default class Routes extends Component {
    render() {
        return (
            <Switch>
                <PrivateRoute path="/" exact component={Home} />
                <Route path="/login" exact component={LoginPage} />
                <Route path="*" component={() => (<div>Página 404</div>)} />
            </Switch>
        )
    }
}

