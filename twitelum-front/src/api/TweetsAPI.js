export const carrega = () => {

    return(dispatch) => {

        fetch(`http://localhost:3001/tweets?X-AUTH-TOKEN=${localStorage.getItem('TOKEN')}`)
        .then(response => response.json())
        .then((tweets) => {
        dispatch({ type: 'CARREGA_TWEETS', tweets: tweets })
        })
    }

}

export const adiciona = (novoTweet) => {
    return dispatch => {
    if (novoTweet) {
        fetch(`http://localhost:3001/tweets?X-AUTH-TOKEN=${localStorage.getItem('TOKEN')}`,
          {
            method: 'POST',
            body: JSON.stringify({ conteudo: novoTweet })
          })
          .then((response) => 
            {return response.json()
            })
          .then((novoTweetRegistradoNoServer) => {
            dispatch({ type: 'ADICIONA_TWEET', tweet: novoTweetRegistradoNoServer })
        })
      }
    }
}

export const remove = (idDoTweet) => {
    return (dispatch) => {
        fetch(`http://localhost:3001/tweets/${idDoTweet}?X-AUTH-TOKEN=${localStorage.getItem('TOKEN')}`,
        {
          method: 'DELETE'
        })
        .then((respostaDoServer) => respostaDoServer.json())
        .then((respostaPronta) => {
            dispatch({ type: 'REMOVE_TWEET', idDoTweet: idDoTweet })
            dispatch({ type: 'REMOVE_TWEET_ATIVO' })
            dispatch({ type: 'ADD_NOTIFICACAO', msg: 'tweet deletado' })
            setTimeout(() => {
              dispatch({ type: 'REMOVE_NOTIFICACAO' })
            }, 5000)

            /*
          const tweetsAtualizados = this.state.tweets.filter((tweetAtual) => 
          tweetAtual._id !== idDoTweet)
  
        this.setState({
          tweets: tweetsAtualizados, 
          tweetAtivo: {}
        })*/
      })

    }
}   

export const like = (idDoTweet) => {
  return(dispatch) => {
    dispatch({ type: 'LIKE', idDoTweet })
    dispatch({ type: 'ADD_NOTIFICACAO', msg: 'alo alo vc deu like' })
    setTimeout(() => {
      dispatch({ type: 'REMOVE_NOTIFICACAO' })
    }, 5000)
  }
}