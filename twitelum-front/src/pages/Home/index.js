import React, { Component, Fragment } from 'react';
import Cabecalho from '../../components/Cabecalho'
import NavMenu from '../../components/NavMenu'
import Dashboard from '../../components/Dashboard'
import Widget from '../../components/Widget'
import TrendsArea from '../../components/TrendsArea'
import Tweet from '../../containers/TweetPadrao'
import Modal from '../../components/Modal'
import PropTypes from 'prop-types'
import * as TweetsAPI from '../../api/TweetsAPI'


class Home extends Component {

  static contextTypes = {
    store: PropTypes.object.isRequired
  }

  constructor() {
    super()
    this.state = {
      novoTweet: '',
      tweets: [],
      tweetAtual: {}
    }
    this.adicionaTweet = this.adicionaTweet.bind(this)
  }

  componentWillMount(){

    this.context.store.subscribe(() => {
      this.setState({
        tweets: this.context.store.getState().tweets.lista,
        tweetAtual: this.context.store.getState().tweets.tweetAtual
      })
    })
  }


  componentDidMount() {
      this.context.store.dispatch(TweetsAPI.carrega())
  }

    adicionaTweet(evento) {
      evento.preventDefault()
      const novoTweet = this.state.novoTweet
      //const tweetsAntigos = this.state.tweets
      this.context.store.dispatch(TweetsAPI.adiciona(novoTweet))

      this.setState({
        novoTweet: ''
      })
    }
    /*
    removeTweet = (idDoTweet) => {
      this.context.store.dispatch(TweetsAPI.remove(idDoTweet))

      this.setState({
        tweetAtual: {}
      })
    }*/


  
  abreModalParaTweet = (idDoTweetQueVaiNoModal, event) => {
    const ignoraModal = event.target.closest('.ignoraModal')

    if (!ignoraModal){
      this.context.store.dispatch({ type: 'ADD_TWEET_ATIVO', idDoTweetQueVaiNoModal })
    }
  }

  fechaModal = (event) => {
    const isModal = event.target.classList.contains('modal')
    if (isModal) {
      this.context.store.dispatch({ type: 'REMOVE_TWEET_ATIVO' })
    }
  }
  

    // validaEnquantoUsuarioDigita() {
    //   validaCaracteres()
    //   quantidadeDeChar()
    //   desativaOBotaO()      
    // }

    render() {
      console.log('render')
      return (
        <Fragment>
          <Cabecalho>
            <NavMenu usuario="@omariosouto" />
          </Cabecalho>
          <div className="container">
            <Dashboard>
              <Widget>
                <form className="novoTweet" onSubmit={this.adicionaTweet}>
                  <div className="novoTweet__editorArea">
                    <span className={`novoTweet__status ${
                      this.state.novoTweet.length > 140
                        ? 'novoTweet__status--invalido' : ''
                      }`}>
                      {this.state.novoTweet.length}/140
                            </span>
                    <textarea
                      value={this.state.novoTweet}
                      onChange={(event) => { this.setState({ novoTweet: event.target.value }) }}
                      className="novoTweet__editor"
                      placeholder="O que está acontecendo?"></textarea>
                  </div>
                  <button type="submit"
                    disabled={
                      this.state.novoTweet.length > 140
                        ? true : false
                    }
                    className="novoTweet__envia">Tweetar</button>
                </form>
              </Widget>
              <Widget>
                <TrendsArea />
              </Widget>
            </Dashboard>
            <Dashboard posicao="centro">
              <Widget>
                <div className="tweetsArea">
                  {this.state.tweets.map(
                    (tweetInfo, index) =>
                      <Tweet
                        key={tweetInfo._id}
                        texto={tweetInfo.conteudo}
                        handleModal={(event) => this.abreModalParaTweet(tweetInfo._id, event)}
                        tweetInfo={tweetInfo} />
                  )}
                </div>

              </Widget>
            </Dashboard>
          </div>

{

  this.state.tweetAtual._id &&
  <Modal isAberto={this.state.tweetAtual._id} fechaModal={this.fechaModal}>
    <Widget>
      <Tweet
        removeHandler={() => this.removeTweet(this.state.tweetAtual._id)}
        texto={this.state.tweetAtual.conteudo}
        tweetInfo={this.state.tweetAtual} />
    </Widget>
  </Modal>
}

{
  this.context.store.getState().notificacao &&
  <div className="notificacaoMsg" onAnimationEnd={() => this.context.store.dispach({ type: 'REMOVE_NOTIFICACAO'}) }
  > {this.context.store.getState().notificacao}</div>

}

</Fragment>

      );
    }
  }


  export default Home;
