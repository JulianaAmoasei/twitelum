import { createStore, applyMiddleware, combineReducers } from 'redux'
import thunk from 'redux-thunk'

function tweetsReducer(state = { lista: [], tweetAtual:{} }, action = {}) {
    console.log(action)
    if(action.type === 'CARREGA_TWEETS'){
        const novoEstado = {
            ...state,
            lista: action.tweets
        }
        return novoEstado
    }

    if(action.type === 'ADICIONA_TWEET') {
        console.warn('ação que está acontecendo agora:', action.type)

        const novoEstado = {
            ...state,
            lista: [action.tweet, ...state.lista]
        }
        return novoEstado
    }

    if(action.type === 'REMOVE_TWEET') {
        console.log('não faço ideia', state)
        const listaDeTweets = state.lista.filter((tweetAtual) => tweetAtual._id !== action.idDoTweet)
        const novoEstado = {
            ...state,
            lista: listaDeTweets,
            tweetAtual: {}
        }
        return novoEstado
    }

    if(action.type === 'ADD_TWEET_ATIVO') {

        const tweetAtual = state.lista
        .find((tweetAtual) => tweetAtual._id === action.idDoTweetQueVaiNoModal)

        const novoEstado = {
            ...state,
            tweetAtual: tweetAtual
        }
        return novoEstado
    }

    if(action.type === 'REMOVE_TWEET_ATIVO') {
        return {
            ...state,
            tweetAtual: {}
        }
    }

    if(action.type === 'LIKE') {
        const tweetsAtualizados = state.lista.map ((tweetAtual) => {

            if(tweetAtual._id === action.idDoTweet){
                const {likeado, totalLikes} = tweetAtual
                tweetAtual.likeado = !likeado
                tweetAtual.totalLikes = likeado ? totalLikes - 1 : totalLikes + 1
            }

            return tweetAtual
        })
        
        return {
            ...state,
            lista: tweetsAtualizados
        }
    }

    return state
}

function notificacoesReducer(state = '', action = {}) {
    if(action.type === 'ADD_NOTIFICACAO') {
        const novoEstado = action.msg
        return novoEstado
    }

    if(action.type === 'REMOVE_NOTIFICACAO') {
        const novoEstado = ''
        return novoEstado
    }
    
    return state
}

const store = createStore(
    combineReducers({
    tweets: tweetsReducer, 
    notificacao: notificacoesReducer
    }),
    applyMiddleware(thunk)

)

export default store